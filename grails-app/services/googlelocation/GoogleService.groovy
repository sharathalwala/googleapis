package googlelocation

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse

class GoogleService implements GrailsConfigurationAware {

    String base
    String appid


    @Override
    void setConfiguration(Config co) {
        base = co.getProperty('base.url', String, 'https://maps.googleapis.com/maps/api/geocode/json')
        appid = co.getProperty('maps.appid', String)

    }

    def getLocationParam(city,state) {
        def url_string = base + '?address=' +
                URLEncoder.encode(city, "UTF-8") +
                ',' + URLEncoder.encode(state, "UTF-8") +
                "&key=${appid}"
    }
}
