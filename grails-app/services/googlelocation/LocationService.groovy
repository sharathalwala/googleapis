package googlelocation

import com.google.api.client.util.Lists
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.language.v1.AnalyzeEntitiesRequest
import com.google.cloud.language.v1.AnalyzeEntitiesResponse
import com.google.cloud.language.v1.AnalyzeSyntaxRequest
import com.google.cloud.language.v1.AnalyzeSyntaxResponse
import com.google.cloud.language.v1.ClassificationCategory
import com.google.cloud.language.v1.ClassifyTextRequest
import com.google.cloud.language.v1.ClassifyTextResponse
import com.google.cloud.language.v1.Document
import com.google.cloud.language.v1.EncodingType
import com.google.cloud.language.v1.LanguageServiceClient
import com.google.cloud.language.v1.Sentiment
import com.google.cloud.translate.Detection
import com.google.cloud.translate.Translate
import com.google.cloud.translate.Translate.TranslateOption
import com.google.cloud.translate.TranslateException
import com.google.cloud.translate.TranslateOptions
import com.google.cloud.translate.Translation
import com.google.cloud.vision.v1.AnnotateImageRequest
import com.google.cloud.vision.v1.AnnotateImageResponse
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse
import com.google.cloud.vision.v1.Feature
import com.google.cloud.vision.v1.Image
import com.google.cloud.vision.v1.ImageAnnotatorClient
import com.google.cloud.vision.v1.TextAnnotation
import com.google.common.collect.ImmutableList
import com.google.protobuf.ByteString
import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.ling.CoreLabel
import edu.stanford.nlp.pipeline.StanfordCoreNLP
import edu.stanford.nlp.tagger.maxent.MaxentTagger
import edu.stanford.nlp.util.CoreMap
import grails.config.Config
import grails.converters.JSON
import grails.core.support.GrailsConfigurationAware
import grails.gorm.transactions.Transactional
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.transform.AnnotationCollector
import groovy.transform.CompileDynamic
import edu.stanford.nlp.pipeline.Annotation
import groovy.transform.CompileStatic



@Transactional

class LocationService {

    GoogleService googleService


    String base
    String appid

    @Override
    String toString() {
        return super.toString()
    }

    //Google location API

    @CompileDynamic
     def getLocation(city,state) {
        RestBuilder rest = new RestBuilder()
        def url= googleService.getLocationParam(city,state)
        RestResponse restResponse = rest.get(url)
        return restResponse.getBody()
    }


    //Google Translation API
    @CompileDynamic
    def static detectLan(text){
        Translate translate = createTranslateService()
        Detection detections = translate.detect(text)
        return detections
    }
    static Translate createTranslateService() {
        return TranslateOptions.newBuilder().build().getService()
    }




    @CompileDynamic
    def translateTextFromSourceToTarget(text,source,target) {
        try {
            Translation resp = TranslateOptions.defaultInstance.service.translate(text,TranslateOption.sourceLanguage(source),TranslateOption.targetLanguage(target))

            def res=resp.getTranslatedText().trim()

            return res
        } catch (TranslateException e) {
            e.printStackTrace()
        }
    }

    //loop through the object and get collection of values
    def checkObjects(object, list) {
        String val = " "
        object.each {
            key, value ->
                if (value instanceof Map) {
                    checkObjects(value, list)
                }
                if (value instanceof String) {
                    val = "$value"
                    list.addAll(val)

                }
        }
        return list
    }



    //Image Processing API

        @SuppressWarnings(['ReturnNullFromCatchBlock', 'CatchException'])
        String detectDocumentText(InputStream inputStream) {



            try {
                List<AnnotateImageRequest> requests = []

                ByteString imgBytes = ByteString.readFrom(inputStream)

                Image img = Image.newBuilder().setContent(imgBytes).build()

              //  Feature indicates a type of image detection task to perform.
                Feature feat = Feature.newBuilder().setType(Feature.Type.DOCUMENT_TEXT_DETECTION).build()

               // AnnotateImageRequest class that specifies how to parse/serialize into the JSON that is transmitted over HTTP when working with the Cloud Vision API
                AnnotateImageRequest request =
                        AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build()
                requests.add(request)
                //ImageAnnotatorClient provides the ability to make remote calls to the backing service through method
                // * calls that map to API methods

                BatchAnnotateImagesResponse response = ImageAnnotatorClient.create().batchAnnotateImages(requests)
                List<AnnotateImageResponse> responses = response.responsesList

                List<String> responsesText = []
                for (AnnotateImageResponse res : responses) {
                    if (res.hasError()) {
                        continue
                    }

                  //TextAnnotation contains a structured representation of OCR extracted text.

                    TextAnnotation annotation = res.fullTextAnnotation
                    responsesText << annotation.text
                }
               /* return responsesText*.replaceAll('\n', ' ').join(' ').trim().replaceAll(' +', ' ')*/
                return responsesText

            } catch ( Exception e ) {
            e.printStackTrace()
            }
        }

    //Natural language processing API

    @CompileDynamic
    def getTexts(text){
        def result=[]
        def details
        def json

    // creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution
    Properties props = new Properties()



    props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref")

    StanfordCoreNLP pipeline = new StanfordCoreNLP(props)
     // create an empty Annotation just with the given text
    Annotation document = new Annotation(text)
    // run all Annotators on this text
    pipeline.annotate(document)

    List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class)

        sentences.each { CoreMap sentence ->

            // traversing the words in the current sentence
            // a CoreLabel is a CoreMap with additional token-specific methods
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {

                // this is the text of the token
                String word = token.get(CoreAnnotations.TextAnnotation.class)
                // this is the POS tag of the token
                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class)
                // this is the NER label of the token
                String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class)


               details = [word:word, pos:pos, ne:ne]

                result.add(details.collect())


                }

            json = new JsonBuilder(result).toPrettyString()

            }
        return json
        }


    //Google NLP api

    def getInformation(String text){

        def result=[]
        def details

            LanguageServiceClient language = LanguageServiceClient.create()


            // The text to analyze
            Document doc = Document.newBuilder()
                    .setContent(text).setType(Document.Type.PLAIN_TEXT).build()

            // Detects the sentiment of the text
            Sentiment sentiment = language.analyzeSentiment(doc).documentSentiment

        def sentimentScore=sentiment.score
        def sentimentMagnitude=sentiment.magnitude

        println('*********************'+sentimentScore+'************'+sentimentMagnitude)

        //Detects the entity analysis
        AnalyzeEntitiesRequest req = AnalyzeEntitiesRequest.newBuilder()
                .setDocument(doc)
                .setEncodingType(EncodingType.UTF16)
                .build()

        AnalyzeEntitiesResponse resp = language.analyzeEntities(req)

        resp.getEntitiesList().each {entity->
            def entityName=entity.getName()
            def entityType=entity.getType()

            def entitySalience=entity.getSalience()
            entity.getMetadataMap().entrySet().each { entry->
               def key=entry.getKey()
                def value=entry.getValue()
                def mapDetails=[key:key,value:value]
                result.addAll(mapDetails.collect())
            }
            entity.getMentionsList().each {mention->
                def textOffset=mention.getText().getBeginOffset()
                def content=mention.getText().getContent()
                def type=mention.getType()
                def mentionDetails=[textOffset:textOffset,content:content,type:type]
                result.addAll(mentionDetails.collect())
            }
            def entityDetails=[entityName:entityName,entityType:entityType,entitySalience:entitySalience]
        result.addAll(entityDetails.collect())
        }

        def request = ClassifyTextRequest.newBuilder().setDocument(doc).build()

        // detect categories in the given text with content classification
        ClassifyTextResponse response = language.classifyText(request)


        response.getCategoriesList().each {category->
            def catName=category.getName()
            def catConfidence=category.getConfidence()

            println(catConfidence+'****************'+catName)

            details = [catName:catName, catConfidence:catConfidence,sentimentScore:sentimentScore,sentimentMagnitude:sentimentMagnitude]

            result.add(details.collect())
        }
        return new JsonBuilder(result).toPrettyString()
    }
    @CompileDynamic
    def getMedicalInfo(text){
       def url = "https://api.lexigram.io/v1/lexigraph/search"
        RestBuilder rest = new RestBuilder()
        def url_string = url +'?q='+ URLEncoder.encode(text,"UTF-8")+
                "&key=${appid}"
        RestResponse restResponse = rest.get(url_string).setProperty("Authorization","Bearer Token"+"access token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdSI6Imx4ZzphcGkiLCJzYyI6WyJrZzpyZWFkIiwiZXh0cmFjdGlvbjpyZWFkIl0sImFpIjoiYXBpOjI1YzdhYzVhLWMxNWEtOTQwNy05ZGVmLTk5ZTU2Yjc1NzAyOSIsInVpIjoidXNlcjpiODYyOTM0My05ZTg5LWJmNDktYWMwNi1iMDBiOWE5ZDY4M2UiLCJpYXQiOjE1MzIzMzkwMDd9.FcqWBzZ0Tqpbgk_ehxAlC58YhYlkpLlcfSmz3ARsHgk")
        return restResponse.getBody()

    }




}


