package googlelocation

import com.google.cloud.language.v1.PartOfSpeech
import com.google.cloud.speech.v1.RecognitionAudio
import com.google.cloud.speech.v1.RecognitionConfig
import com.google.cloud.speech.v1.RecognizeResponse
import com.google.cloud.speech.v1.SpeechClient
import com.google.cloud.speech.v1.SpeechRecognitionAlternative
import com.google.cloud.speech.v1.SpeechRecognitionResult
import com.google.cloud.translate.Detection
import com.google.cloud.translate.Translate
import com.google.cloud.translate.TranslateOptions
import com.google.cloud.translate.Translation
import com.google.common.collect.ImmutableList
import com.google.gson.JsonObject
import com.google.protobuf.ByteString
import edu.stanford.nlp.io.EncodingPrintWriter
import grails.rest.*
import grails.converters.*
import groovy.json.JsonSlurper
import groovyjarjarantlr.collections.List
import org.apache.tomcat.util.http.fileupload.FileUtils
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths


class LocationController extends RestfulController {
    LocationService locationService

    static responseFormats = ['json', 'xml']
    static allowedMethods = [speechTxt: 'GET', getMedicalData: 'POST', getNLPInfo: 'POST', getCoordinates: 'POST', getTranslatedLanguage: 'POST', getTextFromImage: ['POST', 'GET'], getTokens: ['GET', 'POST']]


    LocationController() {
        super(Location)
    }

    def getCoordinates() {

        def resp = locationService.getLocation(params.city, params.state)
        render JSON.parse(resp.toString()) as JSON
    }

    /*def getTranslatedLanguage() {

        def response=locationService.translateTextFromSourceToTarget(params.text,params.source,params.target)
         render response

    }*/

    /* def getTranslatedLanguage() {

      MultipartFile doc=params.file
        def fileContents=doc.getInputStream().getText()

        def response=locationService.translateTextFromSourceToTarget(fileContents,params.source,params.target)
        render response

    }*/

    def getTextFromImage() {

        MultipartFile fil = params.file
        try {

            InputStream fis = new ByteArrayInputStream(fil.getBytes())
            def text = locationService.detectDocumentText(fis)

            File f = new File('myfile.txt')
            f.append(text)

            render text
        } catch (IOException e) {
            e.printStackTrace()
        }
    }

    def getTokens() {

        def response = locationService.getTexts(params.text)
        render JSON.parse(response.toString()) as JSON
    }


    def getNLPInfo() {
        def response = locationService.getInformation(params.text)
        render JSON.parse(response.toString()) as JSON

    }

    def getMedicalData() {
        def response = locationService.getMedicalInfo(params.text)
        render JSON.parse(response.toString()) as JSON

    }


    def getTranslatedLanguage() {
        def list = []

        //loads json file
        MultipartFile doc = params.file

        def fileContents = doc.getInputStream().getText()

        //parse json file
        def jsonSlurper = new JsonSlurper()
        def object = jsonSlurper.parseText(fileContents)

        //get list of values to be translate
        list = locationService.checkObjects(object, list)

        String text = ""
        for (String data : list) {
            text = text + "," + data
        }

        text = text.replaceFirst(",", "")

        //translate all the values to other language
        def respo = locationService.translateTextFromSourceToTarget(text, params.source, params.target).replace(' ', '')

        String[] textValues = text.split(",")
        String[] responseValue = respo.split(",")

        def responseMap = [:]
        def responseFile = fileContents

        //add translated words to all the input keys and replace them
        for (int i = 0; i < textValues.length; i++) {
            responseMap.put(textValues[i], responseValue[i])
            responseFile = responseFile.replace(textValues[i], responseMap.get(textValues[i]).toString())
        }
        def jsonData = JSON.parse(responseFile.toString()) as JSON
        render jsonData

        File f = new File('en_dutch.json')
        f.append(jsonData)
    }


    def speechTxt(){
        SpeechClient speechClient = SpeechClient.create()

            // The path to the audio file to transcribe
            String fileName = "C:\\Users\\sharath.alwala\\Downloads\\male.wav"

            // Reads the audio file into memory
            Path path = Paths.get(fileName)
            byte[] data = Files.readAllBytes(path)
            ByteString audioBytes = ByteString.copyFrom(data)

            // Builds the sync recognize request
            RecognitionConfig config = RecognitionConfig.newBuilder()
                    .setEncoding(RecognitionConfig.AudioEncoding.LINEAR16)
                    .setSampleRateHertz(8000)
                    .setLanguageCode("en-US")
                    .build()
            RecognitionAudio audio = RecognitionAudio.newBuilder()
                    .setContent(audioBytes)
                    .build()

            // Performs speech recognition on the audio file
            RecognizeResponse response = speechClient.recognize(config, audio)
            def results = response.getResultsList()
             SpeechRecognitionAlternative alternative
        for (SpeechRecognitionResult result : results) {
                // There can be several alternative transcripts for a given chunk of speech. Just use the
                // first (most likely) one here.
                 alternative = result.getAlternatives(0)
               //println("@@#@#@ Transcription: ", alternative.getTranscript())

            }

        render alternative.getTranscript()
    }


}