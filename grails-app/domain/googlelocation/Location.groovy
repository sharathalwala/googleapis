package googlelocation


import grails.rest.*
import org.springframework.web.multipart.MultipartFile

@Resource(uri = "/location" ,readOnly = false, formats = ['json', 'xml'])
class Location {
    String city
    String state
    String text
    String source
    String target
    String file

}