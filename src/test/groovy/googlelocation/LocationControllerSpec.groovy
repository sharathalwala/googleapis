package googlelocation

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class LocationControllerSpec extends Specification implements ControllerUnitTest<LocationController> {

   /* def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        expect:"fix me"
        true == false
    }*/

    def  "test Location"(){

        given:
        String city = 'Bangalore'
        String state = 'Karnataka'
        controller.locationService = Stub(LocationService) {
            getLocation(_,_) >> new Location(city: city, state: state)

                    }


        when: 'request is sent with city and state'

        request.method = 'POST'
        request.contentType = FORM_CONTENT_TYPE
        params['city'] =  city
        params['state'] = state

        controller.getCoordinates()




        then: 'CREATED status code is set'
        response.status == 200


    }

    def "test Natural language Processing"(){

        given:
        String text = 'What is the Weather in Bangalore right now'

        controller.locationService = Stub(LocationService) {
            getTexts() >> new Location(text: text)

        }

        when: 'request is sent with sentence'

        request.method = 'GET'
        params['text'] =  text
        controller.getTokens()

        then: 'CREATED status code is set'
        response.status == 200
    }

    def "test language translation"(){
        given:

        String text='What is the Weather in Bangalore right now'
        String source='en'
        String target='es'

        controller.locationService = Stub(LocationService) {
            translateTextFromSourceToTarget(_,_,_) >> new Location(text: text, source: source,target: target)

        }

        when: 'request is sent with text'

        request.method = 'POST'
        params['text'] =  text
        params['source'] =  source
        params['target'] =  target
        controller.getTranslatedLanguage()

        then: 'status code is set'
        response.status == 200
    }

     def "test image prosessing API"(){

 FileInputStream fis = new FileInputStream("C://MAC.png")


       controller.locationService = Stub(LocationService) {
           detectDocumentText() >> new Location(file: file)

       }

       when: 'request is sent with text'

       request.method = 'POST'
       controller.getTextFromImage()


       then: 'status code is set'
      response.status == 200
    }

}
